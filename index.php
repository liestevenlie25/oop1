<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal ("Shaun");

echo "Nama Hewan = " . $sheep->merk . "<br>";
echo "Jumlah Kaki = " . $sheep->legs . "<br>";
echo "Cold Blooded = " . $sheep->cold_blooded . "<br>";
echo "<br>";
$kodok = new Frog ("Buduk");
echo "Nama Hewan = " . $kodok->merk . "<br>";
echo "Jumlah Kaki = " . $kodok->legs . "<br>";
echo "Cold Blooded = " . $kodok->cold_blooded . "<br>";
echo "Jump = " . $kodok->jump . "<br>";
echo "<br>";
$sungokong = new Ape ("Kera Sakti");
echo "Nama Hewan = " . $sungokong->merk . "<br>";
echo "Jumlah Kaki = " . $sungokong->legs . "<br>";
echo "Cold Blooded = " . $sungokong->cold_blooded . "<br>";
echo "Jump = " . $sungokong->yell . "<br>";
?>